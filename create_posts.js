'use strict';

// Librería para hacer preguntas
const readline = require('readline');

// Si no incluimos la carga de las variables de entorno da un error ya que el código de conexión
// requiree esos valores
require('dotenv').config();

const posts = require('./data/posts.json').posts;
const conn = require('./lib/connectMongoose');
const Post = require('./models/Post');

// Cuando se abra la conexión de la base de datos quiero realizar la pregunta
conn.once('open', async () => {
    try {
        const response = await askUser('Estas seguro que quieres borrar los posts introducidos en la base de datos bloggio? (si/no): ');

        if(response.toLowerCase() !== 'si') {
            console.log('Proceso abortado');
            process.exit();
        }

        await initPosts(posts);

        // Cerrar la conexión
        conn.close();
    } catch (err) {
        console.log('Hubo un error ', err);
        process.exit(1);
    }
});

function askUser(question) {
    return new Promise((resolve, reject) => {
        const rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });

        rl.question(question, (answer) => {     
            rl.close();
            resolve(answer);
        });
    });
}

async function initPosts(posts) {
    console.log('init');
    // Eliminar los documentos actuales
    const deleted = await Post.deleteMany();
    console.log('Deleted ' + deleted.n + ' posts');
    // Cargar los nuevos documentos
    const inserted = await Post.insertMany(posts);
    console.log('Inserted ' + inserted.length + ' posts');
}