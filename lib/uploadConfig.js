'use strict';

const multer = require('multer');
const path = require('path');
const fs = require('fs');


// Multer upload configuration
const storage = multer.diskStorage({

    destination: (req, file, cb) => {

        //// Separamos los uploads en una carpeta de cada usuario, primero verificamos si existe, y si no la creamos.
        const dir = path.join(__dirname, '..', 'public', 'images', 'posts');

        // TODO: Error in prod server
        // if (!fs.existsSync(dir)){
        //     fs.mkdirSync(dir);
        // }

        // Aquí podría elegir la ruta dinamicamente donde guardarlo
        cb(null, dir);
    },
    // Si no ponemos esta opción, subirá los ficheros con un nombre aletorio para impedir colisiones de nombres
    filename: (req, file, cb) => {
        cb(null, req.apiUserId + '-' + file.fieldname + '-' + Date.now() + '-' + file.originalname.trim().replace(/ /gi, "-"));
    }
});

module.exports = multer({ storage: storage });