'use strict';

var express = require('express');
var router = express.Router();
var createError = require('http-errors');

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('../../models/User');

const jwtAuth = require('../../lib/jwtAuth');

router.post('/authenticate', async (req, res, next) => {
    try {
        // recoger parámetros del cuerpo de la petición
        const nick = req.body.nick;
        const password = req.body.password;

        //buscar el user
        const user = await User.findOne({
            nick: nick
        });
        
        if (!user || !await bcrypt.compare(password, user.password)) {
            res.json({
                success: false,
                error: 'Invalid credentials'
            })
            return;
        }
        // user encontrado y password ok
        // no meter instancias de mongoose en el token!
        jwt.sign({
            _id: user._id
        }, process.env.JWT_SECRET, {
            expiresIn: '7200s'
        }, (err, token) => {
            if (err) {
                next(err);
                return;
            }
            res.json({
                success: true,
                token: token,
                user: user
            });
        });
    } catch (err) {
        next(err);
    }
});

/**
 * GET /:nick
 * Return a user
 */

 router.get('/', jwtAuth(), async(req, res, next) => {
    try {
        const _id = req.apiUserId;

        //buscar el user
        const user = await User.findById(_id).exec();

        if(!user) {
            next(createError(404));
            return;
        }
        
        res.json({ success: true, result: user });

    } catch (err) {
        next(err)
    }
 });

/**
 * POST /
 * Create a user
 */
router.post('/', async(req, res, next) => {
    try {
        const userDatas = req.body;


        // crear un usuario en memoria
        const user = new User(userDatas);
        user.password = await User.hashPassword(user.password);
       
        //buscar si existe un usuario con el mismo nick o password.
        const foundUserByNick = await User.findOne({
            nick: user.nick
        }).exec();
        const foundUserByEmail = await User.findOne({
            email: user.email
        }).exec();
        

        if(foundUserByNick || foundUserByEmail) {
            res.json({
                success: false,
                error: 'User already register with same nick or email. Please, try another one'
            })
            return;
        }

  
        // guardarlo en la base de datos
        const userSaved = await user.save();

        res.json({ success: true, user: userSaved });
    } catch (err) {
        next(err);
    }    
});

/**
 * DELETE /
 * 
 * Delete user
 */

 router.delete('/', jwtAuth(), async(req, res, next) => {
    try {
        const _id = req.apiUserId;

        await User.remove({ _id: _id }).exec();

        res.json({ success: true });
    } catch (err) {
        next(err);
    }    
});

/**
 * PUT /
 * Update a user
 */
router.put('/', jwtAuth(), async(req, res, next) => {
    try {
        const _id = req.apiUserId;
        const userDatas = req.body;
        // Si no pasamos la opción new: true para que nos devuelva el nuevo registro actualizado. Si no se especifica,
        // nos devolverá el registro anterior a la actualización
        const userUpdated = await User.findByIdAndUpdate(_id, userDatas, { new: true }).exec();

        res.json({ success: true , user: userUpdated });
    } catch (err) {
        next(err);
    }    
});

module.exports = router;