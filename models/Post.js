'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Category = require('../models/Category');

const states = {
    values: ['PUBLISHED', 'DRAFT'],
    message: '{VALUE} is not a valid state'
}

const postSchema = new Schema({
    title: { type: String, required: true },
    video_url: String,
    img_url: String,
    intro_text: { type: String, required: true },
    body_text: { type: String, required: true },
    state: { type: String, required: true, enum: states },
    date_insert: Date,
    date_publish: { type: Date, required: true },
    path: { type: String, required: true, unique: true },
    category: { type: Schema.Types.ObjectId, ref: 'Category' , required: true }, //esto deberia ser un array??
    author: { type: Schema.Types.ObjectId, ref: 'User' }
});

postSchema.statics.getListPublished = async function (req) {
    console.log(req);
    const state = 'PUBLISHED';
    const category = req.query.category;
    const author = req.query.author;

    const limit = parseInt(req.query.limit) || 8;
    const skip = parseInt(req.query.start);
    const sort = req.query.sort; 
    const page = req.query.page;

    const filter = {};

    if (category) {
        const category_path = await Category.findOne({path: category}).exec();
        filter.category = category_path;
    }
    if (author) {
        filter.author = author;
    }
    filter.state = state;

    return Post.doQuery(filter, limit, skip, sort, page);
}

postSchema.statics.doQuery = function (filter, limit, skip, sort, page) {
    
    const query = Post.find(filter);
    query.limit(limit);
    if (page){
        query.skip(limit * (page-1));
    } else {
        query.skip(skip);
    }    
    query.sort(sort);
    query.populate('category');
    query.populate('author');
    return query.exec();
}

const Post = mongoose.model('Post', postSchema);

module.exports = Post;