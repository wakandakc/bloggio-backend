'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const categorySchema = new Schema({
    title: String,
    description: String,
    path: { type: String, unique: true }
});

categorySchema.statics.getAll = async function () {
    return Category.doQuery(null);
}

categorySchema.statics.doQuery = function (filter) {    
    const query = Category.find(filter);
    return query.exec();
}

const Category = mongoose.model('Category', categorySchema);

module.exports = Category;