var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const session = require('express-session');
var cors = require('cors');

// Integración con session
const MongoStore = require('connect-mongo')(session);

const { isAPI } = require('./lib/utils');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));

// Así puedo poner mis plantillas en extensión html
app.set('view engine', 'html');
app.engine('html', require('ejs').__express);

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(cors());
app.options('*', cors());

// para servir ficheros estáticos
app.use(express.static(path.join(__dirname, 'public')));

// Configuramos multiidioma en express
const i18n = require('./lib/i18nConfigure')();
app.use(i18n.init);

// Variables globales de templates
app.locals.title = 'Bloggio';

/**
 * Conexión con la base de datos a través de Mongoose y registro de modelos
 */
require('./lib/connectMongoose');
require('./models');

/**
 * Inicializamos/cargamos la sesión del usuario que hace la petición
*/
app.use(session({
  name: 'bloggio-session',
  secret: '2ncr\LeQ1N0TBkC',
  resave: false,
  saveUninitialized: false,
  cookie: { maxAge: 2 * 24 * 60 * 60 *1000, httpOnly: true  }, // Con httpOnly sólo se puede acceder 
  // desde el servidor a través del protocolo HTTP. Nunca a través del navegador
  store: new MongoStore({
    // como conectarse a la base de datos
    url: process.env.MONGOOSE_CONNECTION_STRING
  })
}));

// Auth Helper Middleware - acceso a sesión desde las vistas
app.use((req, res, next) => {
  res.locals.session = req.session;
  next();
});

/**
 * Rutas de mi API
 */
// Donde se tenga un método .use se le tiene que pasar un middleware y no un Objeto
// esto se debe a que no se ha exportado el módulo con module.exports = router; al final
app.use('/apiv1/users', require('./routes/apiv1/users'));
app.use('/apiv1/posts', require('./routes/apiv1/posts'));
app.use('/apiv1/categories', require('./routes/apiv1/categories'));

/**
 * Rutas de mi aplicación web
 */
app.use('/', require('./routes/blog'));
app.use('/', require('./routes/index'));
app.use('/lang', require('./routes/lang'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

app.use(function(req, res, next) {
 
  next();
});

// error handler: lo modificamos para procesar los errores de la API de forma diferente
app.use(function(err, req, res, next) {
  // Error de validación
  if(err.array) {
    err.status = 422;
    const errorInfo = err.array({onlyFirstError:true})[0];
    err.message = isAPI(req) ? 
      { message: 'Not valid', errors: err.mapped() }
      : `Not valid - ${errorInfo.param} ${errorInfo.msg}`;
  }

  res.status(err.status || 500);

  // Identificamos si el error viene de la API para que responda un JSON y no un código HTML
  if(isAPI(req)) {
    res.json({ success: false, error: err.message });
    return;
  }

  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.render('error');
});

module.exports = app;
