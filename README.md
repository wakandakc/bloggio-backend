# Bloggio

## Install

```shell
npm install
```

Copy .env.example into .env and rewrite the values according to your project

## Database

To initialize the database use:

```shell
npm run install_db
```

If you want, you can create some post with:

```shell
npm run create_posts
```

Pay attention¡ You must create the database before running this script.

## Run

To start the application in production use:

```shell
npm start
```

## Development

To run the application in development use:

```shell
npm run dev
```

## DEMO

You can see a production demo in https://www.bloggio.org/

## React Repository

https://gitlab.com/wakandakc/bloggio-admin
