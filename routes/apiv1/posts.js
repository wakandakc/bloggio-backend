'use strict';

const express = require('express'),
    router = express.Router(),
    jwtAuth = require('../../lib/jwtAuth'),
    Post = require('../../models/Post'),
    Category = require('../../models/Category'),
    upload = require('../../lib/uploadConfig'),
    moment = require('moment');


router.post('/', jwtAuth(), upload.single('image'), async (req, res, next) => {
    
    try {
        const category = await Category.findOne();
    
        const post = new Post({
            title: req.body.title,
            video_url: req.body.video_url,
            intro_text: req.body.intro_text,
            body_text: req.body.body_text,
            state: req.body.state,
            date_insert: new Date(),
            date_publish: moment( req.body.date_publish ).toDate(),
            path: req.body.path,
            category: category._id,
            author: req.apiUserId
        });

        if (req.file && req.file.path) {
            post.img_url = (req.file.path.split('/public/')[1] || req.file.path.split('\\public\\')[1]); //added split for windows path in local option
        }

        const savedPost = await post.save();


        const postUrl = category.path + '/' + savedPost.path

        res.status(200).json({
            success: true,
            result: {
                id: savedPost._id,
                url: postUrl
            }
        });


    } catch (ex) {
        next(ex);
    }
});

router.get('/:id', jwtAuth(), async (req, res, next) => {


    try {

        const post = await Post.findOne({id: req.params.id}).exec();

        res.status(200).json({
            success: true,
            result: post
        })

    } catch(ex) {
        next(ex);
    }

});

router.get('/', jwtAuth(), async (req, res, next) => {

    const skip = Number.parseInt( req.query.skip ) || 0;
    const limit = Number.parseInt( req.query.limit );

    try {
        
        const posts = await Post.find({author: req.apiUserId}).sort('-date_insert').skip(skip).limit(limit).exec();

        res.status(200).json({
            success: true,
            result: posts
        })



    } catch (ex) {
        next(ex);
    }
});

router.delete('/', jwtAuth(), async (req, res, next) => {

    const postId = req.body.id;
    const userId = req.apiUserId;

    try {

        const deletedPost = await Post.findOneAndRemove({author: userId, _id: postId}).exec();

        if (!deletedPost) {
            res.status(400).json({
                success: false,
                result: 'Post not found'
            })
        } else {

            res.status(200).json({
                success: true,
                result: deletedPost
            })

        }

    } catch (ex) {
        next(ex);
    }

});

router.put('/', jwtAuth(), upload.single('image'), async (req, res, next) => {

    const postId = req.body.id;
    const userId = req.apiUserId;

    try {

        const post = await Post.findOne({author: userId, _id: postId}).exec();

        const category = await Category.findOne();


        if (!post) {
            res.status(400).json({
                success: false,
                result: 'Post not found'
            })
        } else {

            post.title = req.body.title;
            post.video_url = req.body.video_url;
            post.intro_text = req.body.intro_text;
            post.body_text = req.body.body_text;
            post.state = req.body.state;
            post.date_publish = moment( req.body.date_publish ).toDate();
            post.category = category._id;
            post.path = req.body.path;

            if (req.file && req.file.path) {
                post.img_url = (req.file.path.split('/public/')[1] || req.file.path.split('\\public\\')[1]); //added split for windows path in local option
            }

            const savedPost = await post.save();

            res.status(200).json({
                success: true,
                result: savedPost
            })

        }

    } catch (ex) {
        console.log(ex);
        next(ex);
    }

});

module.exports = router;