'use strict';

// Librería para hacer preguntas
const readline = require('readline');

// Si no incluimos la carga de las variables de entorno da un error ya que el código de conexión
// requiree esos valores
require('dotenv').config();

const categories = require('./data/categories.json').categories;
const users = require('./data/users.json').users;
const conn = require('./lib/connectMongoose');
const Category = require('./models/Category');
const User = require('./models/User');

// Cuando se abra la conexión de la base de datos quiero realizar la pregunta
conn.once('open', async () => {
    try {
        const response = await askUser('Estas seguro que quieres borrar los contenidos de la base de datos bloggio? (si/no): ');

        if(response.toLowerCase() !== 'si') {
            console.log('Proceso abortado');
            process.exit();
        }

        await initCategories(categories);
        await initUsers(users);

        // Cerrar la conexión
        conn.close();
    } catch (err) {
        console.log('Hubo un error ', err);
        process.exit(1);
    }
});

function askUser(question) {
    return new Promise((resolve, reject) => {
        const rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        });

        rl.question(question, (answer) => {
            // console.log(`Respuesta: ${answer}`);        
            rl.close();
            resolve(answer);
        });
    });
}

async function initCategories(categories) {
    console.log('init');
    // Eliminar los documentos actuales
    const deleted = await Category.deleteMany();
    console.log('Deleted ' + deleted.n + ' categories');
    // Cargar los nuevos documentos
    const inserted = await Category.insertMany(categories);
    console.log('Inserted ' + inserted.length + ' categories');
}


async function initUsers(users) {
    // Eliminar los documentos actuales
    const deleted = await User.deleteMany();
    console.log('Deleted ' + deleted.n + ' users');
    // Hacer hash de las passwords
    for (let i = 0; i < users.length; i++) {
        users[i].password = await User.hashPassword(users[i].password);        
    }
    // Cargar los nuevos documentos
    const inserted = await User.insertMany(users);
    console.log('Inserted ' + inserted.length + ' users');
}