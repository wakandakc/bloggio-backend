var express = require('express'),
    router = express.Router(),
    Post = require('../models/Post'),
    Category = require('../models/Category'),
    moment = require('moment');

const i18n = require('../lib/i18nConfigure')();



router.get('/:category/:path', async (req, res, next) => {

    const categories = await Category.getAll();

    const categoryIn = req.params.category;
    const pathIn = req.params.path;
    console.log(req.headers)

    try {

        const category = await Category.findOne({path: categoryIn}).exec();

        if (!category) {
            next();
        } else {

            const post = await Post.findOne({category: category._id, path: pathIn}).populate('author').populate('category').exec()
    
            if (!post) {
                next();
            } else {
                post.dateParsed = moment(post.date_publish).format('LL');
                res.render('post', 
                {
                    _host: req.headers.host.includes('localhost' | '127.0.0.1') ? 'http://' + req.headers.host : 'https://' + req.headers.host,
                    post: post,
                    categories: categories,
                    breadcrumbs: [{
                        name: 'Home',
                        url: '/'
                    },
                    {
                        name: category.path,
                        url: `?category=${category.path}`
                    },
                    {
                        name: post.path,
                        url: `${category.path}/${post.path}`
                    }]
                });
    
            }
        }

    } catch (ex) {
        next(ex);
    }
    
});


module.exports = router;