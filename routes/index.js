var express = require('express');
var router = express.Router();
var Post = require('../models/Post');
var Category = require('../models/Category');
var moment = require('moment');


const i18n = require('../lib/i18nConfigure')();

const jwtAuthSSO = require('../lib/jwtAuthSSO');



router.get('/loginsso', jwtAuthSSO(), (req, res, next) => {
    res.redirect(req.headers.referer.split('login')[0]);
});

router.get('/logoutsso', (req, res, next) => {
    // Regeneramos la sesión
    req.session.regenerate(function(err) {
        if(err) {
            next(err);
            return;
        }
        res.redirect(req.headers.referer.split('admin')[0]);
    });
});

/**
 * Home: página inicial
 */
router.get('/', async (req, res, next) => {
    
    try {        
        const posts = await Post.getListPublished(req);
        posts.forEach((post) => {
            post.dateParsed = moment(post.date_publish).format('LL');
            post.body_text_resume = `${post.body_text.substr(0,250)}...`;
        });
        console.log(posts);

        const categories = await Category.getAll();
        console.log(categories);

        res.render('index', {
            _host: req.headers.host.includes('localhost' | '127.0.0.1') ? 'http://' + req.headers.host : 'https://' + req.headers.host,
            posts: posts,
            categories: categories,
            breadcrumbs: [{ 
                name: 'Home',
                url: '/'
            }]
        });
    } catch (err) {
        next(err);
    }    
});


module.exports = router;