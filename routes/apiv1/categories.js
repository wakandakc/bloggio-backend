'use strict';

const express = require('express'),
    router = express.Router(),
    jwtAuth = require('../../lib/jwtAuth'),
    Category = require('../../models/Category');


router.post('/', jwtAuth(), async (req, res, next) => {

    try {

        const category = new Category({
            title: req.body.title,
            description: req.body.description,
            path: req.body.path
        });

        const savedCategory = await category.save();

        res.status(200).json({
            success: true,
            result: savedCategory
        });


    } catch (ex) {
        next(ex);
    }
});

router.get('/', jwtAuth(), async (req, res, next) => {

    const skip = Number.parseInt( req.query.skip ) || 0;
    const limit = Number.parseInt( req.query.limit );

    try {
        
        const category = await Category.find().skip(skip).limit(limit).exec();

        res.status(200).json({
            success: true,
            result: category
        })

    } catch (ex) {
        next(ex);
    }
});

router.put('/', jwtAuth(), async (req, res, next) => {

    const categoryId = req.body.id;
    const userId = req.apiUserId;

    try {

        const category = await Category.findOne({_id: postId}).exec();

        if (!category) {
            res.status(400).json({
                success: false,
                result: 'Post not found'
            })
        } else {

            category.title = req.body.title,
            category.description = req.body.description,
            category.path = req.body.path

            const savedCategory = await category.save();

            res.status(200).json({
                success: true,
                result: savedCategory
            })

        }

    } catch (ex) {
        next(ex);
    }

});

module.exports = router;